# YANA.help initially developed at Versusvirus2020

![YANA logo](https://www.yana.help/assets/yana-tree-noHands.svg)

## Contributing workflow
1. After having cloned this repository use `ng serve -o` to test locally (n.b: you might need a local Google Maps API key for some functionalities ask maintainers).
2. Take an existing issue from the backlog (click on it) and create a merge request (You can rename the branch to something more fitting).
3. Follow the instructions to checkout the branch locally. In your console run.
   ```
   git fetch origin
   git checkout -b "name-of-your-branch" "origin/name-of-your-branch"
   ```
4. Implement your feature or fix the bug and commit your changes.
5. Check the open issue to make sure your merge request satisfies requirements
6. Document and run `npx @compodoc/compodoc -p tsconfig.json` if you want to see the updated documentation locally before merging. Further information https://compodoc.app/guides/getting-started.html
7. Pull the latest changes from the master repository. (Resolve conflicts if necessary. In doubt ask maintainers)
8. Push the changes to your merge request branch. Remove "WIP" from title and assign a maintainer to review and merge your code.
9. If everything is fine your code will be merged to the staging branch where we can review the edits on http://test.yana.help.
10. Finally the maintainers will take care of merging all changes from staging onto master so that they can integrate the main site.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
