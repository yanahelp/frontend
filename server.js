//Install express server
const express = require('express');
const path = require('path');
const forceSsl = require('force-ssl-heroku');

const app = express();
if (process.env.FORCE_SSL) {
    app.use(forceSsl);
}

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/versusvirus2020'));

app.get('/*', function (req, res) {

    res.sendFile(path.join(__dirname + '/dist/versusvirus2020/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);