import { HttpHeaders } from '@angular/common/http';

export const environment = {
  production: true,
  baseUrl: 'https://discourse.yana.help'
};

export const defaultHeaders = new HttpHeaders()
  .set('X-Requested-With', 'XMLHttpRequest')
  .set('Content-type', 'application/json')
  .set('X-CSRF-Token', sessionStorage.getItem('csrf') || 'undefined');