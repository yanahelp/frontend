import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, Scroll, NavigationEnd } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { filter } from 'rxjs/operators';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Yana.help';

  public isMobile: boolean;
  public mobileMenuActive: boolean = false;
  /**
   * variable storing the current component (page) being displayed
   */
  private currentComponent: any;

  /**
   * App component constructor
   * @param router the router handling the page changes
   */
  constructor(router: Router) {
    //TODO make the transition to top smoother and maybe try to find a cleaner way to do it
    //This is the only way I found to go back to top on navigation, because Angular's option of handling it doesn't always work
    //It can be applied to other pages if needed
    /**
     * Detection of all route changes in or to "/community" and scrolling back to the top
     */
    router.events.pipe(
      filter((e): e is NavigationEnd => e instanceof NavigationEnd && e.url.startsWith("/community")) //maybe exclude navigation between create-topic and topic-view
    ).subscribe(e => {
      this.currentComponent.scrollTop = 0;
    });
  }

  /**
   * Method that stores the current component parameters
   * @param event the full component and root context JSON
   */
  onActivate(event){
    console.log(event)
    /**
     * The content parameters are always at the [13] [0] place in the JSON
     */
    this.currentComponent = event.__ngContext__[13][0];
    //TODO see if this is safe and maybe find a better way to do it (if only Angular properly handled that)
  }

  ngOnInit(): void {
    
  }

}
