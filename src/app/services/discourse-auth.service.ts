import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { DiscourseError } from '../interfaces/discourse-error';
import {
  environment,
  // APIHeaders
} from '../../environments/environment';
import { DiscourseAuthUserSuccess } from '../interfaces/discourse-auth-user-success';
import { Observable } from 'rxjs';
import { switchMap, tap, share, shareReplay, map } from 'rxjs/operators';
import { DiscourseSession } from '../interfaces/discourse-session';
import { DiscourseUser } from '../interfaces/discourse-user';

@Injectable({
  providedIn: 'root'
})
export class DiscourseAuthService {

  currentUser: DiscourseUser;

  defaultHeaders = new HttpHeaders()
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('Content-type', 'application/json');

  /**
   * The public observable used to give acess to the current user.
   */
  session: Observable<DiscourseSession>;
  /**
   * Handler for Discourse Authentication within the YANA framework.
   * @param http - The handler for HTTP requests.
   */
  constructor(private http: HttpClient) {

    this.session = this.checkStoredToken();
  }

  /**
   * Creates an account within the YANA discourse framework.
   * @param username - The username of the new user.
   * @param password - The password of the new user (has to be minimum 10 characters).
   * @param [name=''] - [Optional] - The name of the new user (disregard for anonymous accounts).
   * @param email - The email of the new user, it needs to look "legit" because the system checks that and unique.
   */
  async createAccount(username: string, password: string, name: string = '', email: string): Promise<any> {
    const available: boolean = await this.getUsernameAvailable(username);
    let succeeded: boolean;
    let response: DiscourseAuthUserSuccess | DiscourseError;

    if (available) {
      await this.http.post(environment.baseUrl +
        '/users.json',
        {
          name,
          email,
          password,
          username,
          active: true,
          approved: true // automatically approved for now
        },
        // {
        //   headers: APIHeaders
        //     .set('Content-Type', 'application/json')
        // }
      )
        .toPromise()
        .then(
          (value: DiscourseAuthUserSuccess) => { // Success
            succeeded = value.success && value.hasOwnProperty('user_id');
            response = value;
          },
          (err: HttpResponse<DiscourseError>) => { // Error
            succeeded = false;
            console.log(err);
          }
        );
    }

    return { available, succeeded, response };
  }

  /**
   * Checks whether the username has already been taken.
   * @param username - The username to be checked
   */
  async getUsernameAvailable(username: string): Promise<boolean> {
    let response: DiscourseAuthUserSuccess | DiscourseError; // wrong interface ?

    await this.http.get<any>(`${environment.baseUrl}/users/${username}`,
      // {headers: APIHeaders}
    ).toPromise()
      .then(
        (data: DiscourseAuthUserSuccess) => { // Success
          response = data;
        },
        (err: DiscourseError) => { // Error
          response = err;
        }
      );

    return !response.hasOwnProperty('user');
  }

  /**
   * Logs a user into the YANA discourse framework using session cookies.
   * A first request is sent to get a CSRF token and then a second in order to get a
   */
  login(username: string, password: string) {
    console.log('Logging in user', username);

    this.http.get<any>(`${environment.baseUrl}/session/csrf`,
      {
        headers: this.defaultHeaders.set('X-CSRF-Token', sessionStorage.getItem('csrf') || 'undefined')

      }).subscribe(res => console.log(res.csrf));

    console.log(this.defaultHeaders);

    // TODO: Document for example the fact that cookies are only served through https not http
    this.http.get<{ csrf: string; }>(`${environment.baseUrl}/session/csrf`,
      {
        headers: this.defaultHeaders
          .set('X-CSRF-Token', sessionStorage.getItem('csrf') || 'undefined')
          .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8'),
        withCredentials: true
      }).pipe(
        tap(response => console.log(response)),
        tap(response => sessionStorage.setItem('csrf', response.csrf)),
        switchMap(
          () => this.http.post<DiscourseSession>(`${environment.baseUrl}/session`, { login: username, password }, {
            headers: this.defaultHeaders
              .set('X-CSRF-Token', sessionStorage.getItem('csrf'))
              .set('Content-Type', 'application/json')
              .set('Cache-Control', 'no-cache'),
            withCredentials: true
          }
          )
        ),
        tap(session => this.currentUser = session.user),
        shareReplay()
        );
      }

      /**
       * The method that checks whtether the token stored in localstorage ois still valid.
       */
      checkStoredToken(): Observable<DiscourseSession> {
        return this.http.get<DiscourseSession>(`${environment.baseUrl}/session/current.json`,
        {
        headers: this.defaultHeaders
        .set('X-CSRF-Token', sessionStorage.getItem('csrf') || 'undefined')
        .set('Content-Type', 'application/json')
        .set('Cache-Control', 'no-cache'),
        withCredentials: true
      }).pipe(
        map(
          session => {
            return {
              ...session,
              user: session.current_user
            };
          }
          ),
          tap(session => this.currentUser = session.user),
          shareReplay()
      );
  }

  /**
   * Logs a user out of the YANA discourse framework.
   * TODO: Complete
   */
  async logout(): Promise<void> {

  }
}
