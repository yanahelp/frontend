import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { DiscourseTopic } from '../interfaces/discourse-topic';
import { DiscoursePost } from '../interfaces/discourse-post';
import { DiscourseTopicCollection } from '../interfaces/discourse-topic-collection';
import { DiscoursePostLatest } from '../interfaces/discourse-post-latest';
import { DiscourseOrdering } from '../enums/discourse-ordering.enum';
import { DiscourseCategory } from '../enums/discourse-category.enum';
import { environment, defaultHeaders } from 'src/environments/environment';
import { Observable, timer } from 'rxjs';
import { distinctUntilKeyChanged, switchMap } from 'rxjs/operators';
import { DiscourseResponse } from '../interfaces/discourse-response';
// import { APIHeaders } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DiscourseContentService {

  
  /**
   * Handler for Discourse Authentication within the YANA framework.
   * @param http - The handler for HTTP requests.
   */
  constructor(private http: HttpClient) { }

  /**
   * Creates a Topic
   * @param title - The title of the topic. (needs to be two words at least apparently ?)
   * @param raw - body of the topic (20 characters minimum)
   * @param category The category the topic is a part of. (1: No category, 2: "Retour vers le site", 3: "Responsables", 4: "Salon")
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information of the topic created. Otherwise, it returns why the call failed.
   */
  async createTopic(title: string, raw: string, category: DiscourseCategory): Promise<DiscourseResponse> {
    
    let response: DiscourseTopic | any;
    let succeeded: boolean;
    await this.http.post<DiscourseTopic>(environment.baseUrl +
      `/posts.json`,
      {
        title,
        raw,
        category
      },
      {
        withCredentials: true,
        headers: defaultHeaders
      }
      // {
      //   headers: APIHeaders
      //   }
    )
      .toPromise()
      .then(
        (data: DiscourseTopic) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Creates a Post (answer to a topic, private discussion)
   * @param topicId - The id of the topic this post is within.
   * @param raw - The text of the post. supports html tags formatting (minimum 20 characters)
   * @param [replyTo=null] - [Optional] - The number of the post this is a reply to. (number is the order of the post in the topic stream, not id)
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information of the post created. Otherwise, it returns why the call failed.
   */
  async createPost(topicId: number, raw: string, replyTo: number = null): Promise<DiscourseResponse> {
    let response: DiscoursePost;
    let succeeded: boolean;
    await this.http.post<DiscoursePost>(environment.baseUrl +
      `/posts.json`,
      {
        topic_id: topicId,
        raw: raw,
        reply_to_post_number: replyTo
      },
      {
        withCredentials: true,
        headers: defaultHeaders

      }
      // {
      //   headers: APIHeaders
      // }
    )
      .toPromise()
      .then(
        (data: DiscoursePost) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  pollTopic(topicId: number): Observable<DiscourseTopic> {
    return timer(0, 15000).pipe(
      switchMap(_ => this.http.get<DiscourseTopic>(environment.baseUrl +
        `/t/${topicId}.json`,
        { withCredentials: true })
      ),
      distinctUntilKeyChanged('posts_count')
    );
  }

  /**
   * Gets a topic using its id.
   * @param topicId - The id of the topic this post is within.
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information of the topic requested. Otherwise, it returns why the call failed.
   */
  async getTopic(topicId: number): Promise<DiscourseResponse> {
    let response: DiscourseTopic;
    let succeeded: boolean;
    await this.http.get<DiscourseTopic>(environment.baseUrl +
      `/t/${topicId}.json`,
      { withCredentials: true }
    )
      .toPromise()
      .then(
        (data: DiscourseTopic) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Gets a collection of topics that are currently considered at the top (most active / liked)
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information requested. Otherwise, it returns why the call failed.
   */
  async getTopTopics(): Promise<any> {
    let response: DiscourseTopicCollection;
    let succeeded: boolean;
    await this.http.get<DiscourseTopicCollection>(
      `/top.json`,
      { withCredentials: true }
    )
      .toPromise()
      .then(
        (data: DiscourseTopicCollection) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Gets a collection of topics given a certain order
   * @param order - The order that the topics should be gotten by (ie. "default", "created", "activity", etc.)
   * @param [ascending=false] - Whether the order should be reversed.
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information requested. Otherwise, it returns why the call failed.
   */
  async getLatestTopicsByOrder(order: DiscourseOrdering, ascending: boolean = false): Promise<any> {
    let response: DiscourseTopicCollection;
    let succeeded: boolean;
    await this.http.get<DiscourseTopicCollection>(
      `${environment.baseUrl}/latest.json?order=${order}&ascending=${ascending}`, { withCredentials: true }
    )
      .toPromise()
      .then(
        (data: DiscourseTopicCollection) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Gets a collection of posts regardless of topics that have been posted.
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information requested. Otherwise, it returns why the call failed.
   */
  async getLatestPosts(): Promise<any> {
    let response: DiscoursePostLatest;
    let succeeded: boolean;
    await this.http.get<DiscoursePostLatest>(environment.baseUrl +
      `/posts.json`,
      { withCredentials: true }
    )
      .toPromise()
      .then(
        (data: DiscoursePostLatest) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Creates a private topic between 2 or more users (equivalent to a start of a private discussion)
   * @param title - Title of the private message
   * @param target_recipients - The targets of the message (comma separated)
   * @param raw - The text of the post. supports html tags formatting (minimum 20 characters)
   * @param [replyTo=null] - [Optional] - The number of the post this is a reply to. (number is the order of the post in the topic stream, not id)
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information of the topic created. Otherwise, it returns why the call failed.
   */
  async createPrivateDiscussionTopic(title: string, target_recipients: string, raw: string): Promise<any> {
    let response: DiscoursePost;
    let succeeded: boolean;
    await this.http.post<DiscoursePost>(environment.baseUrl +
      `/posts.json`,
      {
        title,
        target_recipients,
        raw,
        archetype: 'private_message',
      },
      // {
      //   headers: APIHeaders
      // }
    )
      .toPromise()
      .then(
        (data: DiscoursePost) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );

    return { succeeded, response };
  }

  /**
   * Gets list of private messages (!not working, requires login!)
   * @param username - The name of the user concerned.
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns the information of the topic requested. Otherwise, it returns why the call failed.
   */
  async getPrivateMessages(username: string): Promise<any> {
    let response: any;
    let succeeded: boolean;
    await this.http.get<any>(
      `${environment.baseUrl}/topics/private-messages/${username}.json`, { withCredentials: true }
    )
    .toPromise()
    .then(
      (data: any) => { // Success
        response = data;
        succeeded = true;
      },
      (err: HttpErrorResponse) => { // Error
        response = err.error.errors;
        succeeded = false;
        console.log(err)
      }
    );

    return {succeeded: succeeded, response: response};
  }

  /**
   * Searches for topics, posts, users
   * @param term - The search parameter (has to be minimum 3 characters)
   * @param [order=null] - [Optional] - order the results of the search
   * @return a JSON containing 'succeeded' (if the call succeeded) and 'response' (the response from the server).
   *  If the call succeeded, the server returns collections of topics, posts and users related to the search. Otherwise, it returns why the call failed.
   */
  async search(term: string, order: string = 'relevance'): Promise<DiscourseResponse> {
    let response: any;
    let succeeded: boolean;
    let url: string;

    if(order == 'relevance') url = `${environment.baseUrl}/search.json?q=${term}`;
    else url = `${environment.baseUrl}/search.json?q=${term}%20order%3A${order.toLowerCase()}`;

    console.log(url);

    await this.http.get<any>(
      url, { withCredentials: true }
    )
    .toPromise()
    .then(
      (data: any) => { // Success
        response = data;
        succeeded = true;
      },
      (err: HttpErrorResponse) => { // Error
        response = err.error.errors;
        succeeded = false;
        console.log(err)
      }
    );
    return { succeeded, response };
  }

  async getUsers(): Promise<any> {
    let response: any;
    let succeeded: boolean;
    await this.http.get<any>(
      `${environment.baseUrl}/directory_items.json?period=all&order=topic_count`, { withCredentials: true }
    )
      .toPromise()
      .then(
        (data: any) => { // Success
          response = data;
          succeeded = true;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error.errors;
          succeeded = false;
        }
      );
    return { succeeded, response };
  }

}
