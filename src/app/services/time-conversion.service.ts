import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to convert a date format to a simpler format based on the time elapsed.
 */
export class TimeConversionService {

  /**
   * TimeConversion service constructor
   */
  constructor() { }

  /**
   * Method to convert a UTC date to a format that has only one unit based on the time elapsed since then.
   * For example, "32 days", "2 hours", "3 seconds"
   * @param date the original date
   * @returns time elapsed since then with its unit
   */
  public calculateDiff(date: string): string {
    //TODO: add years ?
    let currentDate: Date = new Date();
    let dateSent: Date = new Date(date);
    let unit: string = 'day';

    var diffMs = (currentDate.getTime() - dateSent.getTime()); // milliseconds
    let result = Math.floor(diffMs / 86400000); // days
    /**
     * If the difference is less than 24 hours, result will be equal to zero
     * so we need to be more precise and test with hours. We repeat this process until 
     * we get something different than zero and we stop at seconds
     */
    if(result == 0){
      result = Math.floor((diffMs % 86400000) / 3600000); // hours
      unit = 'hour';
      if(result == 0){
        result = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
        unit = 'minute';
        if(result == 0){
          result = Math.round((((diffMs % 86400000) % 3600000) % 60000) / 1000); // seconds
          unit = 'second';
        }
      }
    }

    if(result > 1) unit = unit + 's';

    return result + " " + unit
  }
}
