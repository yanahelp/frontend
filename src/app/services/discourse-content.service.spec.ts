import { TestBed } from '@angular/core/testing';

import { DiscourseContentService } from './discourse-content.service';
import { DiscourseTopic } from '../interfaces/discourse-topic';


describe('DiscourseContentService', () => {
  let service: DiscourseContentService;

  function isTopic(obj: DiscourseTopic | any): obj is DiscourseTopic {
    return (obj as DiscourseTopic).fancy_title !== undefined;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscourseContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get a Topic by Id', () => {
    expect(isTopic(service.getTopic(0))).toBeTruthy();
  });
});
