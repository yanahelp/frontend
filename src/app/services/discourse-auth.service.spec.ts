import { TestBed } from '@angular/core/testing';

import { DiscourseAuthService } from './discourse-auth.service';

describe('DiscourseAuthService', () => {
  let service: DiscourseAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscourseAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
