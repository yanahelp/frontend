import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from '../services/lang.service';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DiscourseAuthService } from '../services/discourse-auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  translate: TranslateService;

  signupForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private lang: LangService,
              private authService: FirebaseAuthService,
              private discourseAuthService: DiscourseAuthService) {
    this.translate = lang.getTranslateService();
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const email = this.signupForm.get('email').value;
    const password = this.signupForm.get('password').value;

    this.authService.createNewUser(email, password).then(
      () => {},
      (error) => {
        this.errorMessage = error;
      }
    ).then(
      () => this.discourseAuthService.createAccount('username', password, 'name', email)
    ).then(() => this.router.navigate(['/']));
  }

  loginUser(event) {
    event.preventDefault();
    // const target = event.target
    // const username = target.getElementById('usename')
    // const password = target.getElementById('password')
    console.log(event);
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }
}
