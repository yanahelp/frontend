import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from '../services/lang.service';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GettingStartedComponent implements OnInit {

  isClicked: boolean = false;
  translate: TranslateService;
  langSelector: any = null;
  ageSelector: any = null;
  cantonSelector: string = null;

  constructor(public lang: LangService) {
    this.translate = lang.getTranslateService();
  }

  onClickLang(button): void {
    if (this.langSelector === button) {
      this.langSelector = null;
    } else {
      this.langSelector = button;
    }
  }

  onClickAge(button): void {
    if (this.ageSelector === button) {
      this.ageSelector = null;
    } else {
      this.ageSelector = button;
    }
  }

  receiveCanton($event) {
    console.log($event);
    this.cantonSelector = $event;
  }

  ngOnInit(): void {

  }

}
