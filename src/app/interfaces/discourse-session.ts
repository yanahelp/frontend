import { DiscourseUser } from './discourse-user';
import { DiscourseUserBadge } from './discourse-user-badge';

export interface DiscourseSession {
    current_user?: any;
    user_badges: any[];
    badges: DiscourseUserBadge[];
    badge_types: any[];
    users: DiscourseUser[];
    user?: DiscourseUser;
}
