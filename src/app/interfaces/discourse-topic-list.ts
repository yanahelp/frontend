import { DiscourseTopicShort } from './discourse-topic-short';

export interface DiscourseTopicList {
  can_create_topic: boolean;
  draft: {};
  draft_key: string;
  draft_sequence: {};
  per_page: number;
  topics: DiscourseTopicShort[];
}
