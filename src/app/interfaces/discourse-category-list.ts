import { DiscourseCategory } from './discourse-category';

export interface DiscourseCategoryList {
  category_list: {
    can_create_category: boolean,
    can_create_topic: boolean,
    draft: boolean,
    draft_key: string,
    draft_sequence: number,
    categories: DiscourseCategory[]
  };
}
