import { DiscoursePost } from './discourse-post';

export interface DiscourseTopic {
  post_stream: {
    posts: DiscoursePost[];
    stream: [
      {}
    ]
  };
  timeline_lookup: [
    {
      number: [
        {}
      ]
    }
  ];
  id: number;
  title: string;
  fancy_title: string;
  posts_count: number;
  created_at: string;
  views: number;
  reply_count: number;
  participant_count: number;
  like_count: number;
  last_posted_at: Date;
  last_poster_username: string;
  visible: boolean;
  closed: boolean;
  archived: boolean;
  has_summary: boolean;
  archetype: 'private_message' | 'regular';
  slug: string;
  category_id: number;
  word_count: {};
  deleted_at: {};
  user_id: number;
  draft: {};
  draft_key: string;
  draft_sequence: {};
  unpinned: {};
  pinned_globally: boolean;
  pinned: boolean;
  pinned_at: string;
  pinned_until: {};
  details: {
    auto_close_at: {};
    auto_close_hours: {};
    auto_close_based_on_last_post: boolean;
    created_by: {
      id: number;
      username: string;
      avatar_template: string
    };
    last_poster: {
      id: number;
      username: string;
      avatar_template: string
    };
    participants: [
      {
        id: number;
        username: string;
        avatar_template: string;
        post_count: number
      }
    ];
    suggested_topics: DiscourseTopic[];
    notification_level: number;
    can_flag_topic: boolean
  };
  highest_post_number: number;
  deleted_by: {};
  actions_summary: [
    {
      id: number;
      count: number;
      hidden: boolean;
      can_act: boolean
    }
  ];
  chunk_size: number;
  bookmarked: {};
}
