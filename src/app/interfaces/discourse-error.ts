export interface DiscourseError {
  errors: string[];
  error_type: string;
}
