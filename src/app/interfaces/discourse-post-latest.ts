import { DiscoursePost } from './discourse-post';

export interface DiscoursePostLatest {
  latest_posts: DiscoursePost[];
}
