export interface DiscourseResponse {
    succeeded: boolean;
    response: any;
}
