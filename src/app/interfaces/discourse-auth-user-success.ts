export interface DiscourseAuthUserSuccess {
  success: boolean;
  active: boolean;
  message: string;
  user_id: number;
}
