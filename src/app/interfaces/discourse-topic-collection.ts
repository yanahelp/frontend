import { DiscourseUser } from './discourse-user';
import { DiscourseTopicList } from './discourse-topic-list';

export interface DiscourseTopicCollection {
    users: DiscourseUser[];
    topic_list: DiscourseTopicList;
}
