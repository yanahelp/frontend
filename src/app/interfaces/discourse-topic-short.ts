export interface DiscourseTopicShort {
    id: number,
    title: string,
    fancy_title: string,
    slug: string,
    posts_count: number,
    reply_count: number,
    highest_post_number: number,
    image_url: { },
    created_at: string,
    last_posted_at: string,
    bumped: boolean,
    bumped_at: string,
    unseen: boolean,
    pinned: boolean,
    unpinned: { },
    excerpt: string,
    visible: boolean,
    closed: boolean,
    archived: boolean,
    bookmarked: { },
    liked: { },
    views: number,
    like_count: number,
    has_summary: boolean,
    archetype: string,
    last_poster_username: string,
    category_id: number,
    pinned_globally: boolean,
    posters: [
        {
            extras: string,
            description: string,
            user_id: number
        }
    ]
}