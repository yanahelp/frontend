export interface DiscourseAuthUser {
  user: {
    id: number;
    username: string;
    name: string;
    last_seen_at: string;
  };
}
