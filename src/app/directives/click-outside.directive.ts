import { Directive, Input, Output, EventEmitter, ElementRef, HostListener, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[appClickOutside]',
})
export class ClickOutsideDirective implements OnInit, OnDestroy {
  @Output() clickOutside: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  private clicksOutside = new Subject();
  private subscription: Subscription;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    this.subscription = this.clicksOutside
      .subscribe((e: boolean) => this.clickOutside.emit(e));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('document:click', ['$event.target'])
  public onListenerTriggered(target: ElementRef) {
    const clickedInside = this.elementRef.nativeElement.contains(target);
    if (!clickedInside) {
      this.clicksOutside.next(!clickedInside);
    }
  }
}
