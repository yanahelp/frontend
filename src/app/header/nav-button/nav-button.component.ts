import { Component, OnInit, Input } from '@angular/core';
import { LangService } from 'src/app/services/lang.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nav-button',
  templateUrl: './nav-button.component.html',
  styleUrls: ['./nav-button.component.css']
})
export class NavButtonComponent implements OnInit {
  translate: TranslateService;

  constructor(public lang: LangService) {
    this.translate = lang.getTranslateService();
  }

  @Input() routerLink: string;
  @Input() text: string;
  @Input() bg: string;
  @Input() tc: string;

  ngOnInit(): void {
  }

}
