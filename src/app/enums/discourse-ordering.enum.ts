export enum DiscourseOrdering {
  DEFAULT = 'default',
  CREATED = 'created',
  ACTIVITY = 'activity',
  VIEWS = 'views',
  POSTS = 'posts',
  CATEGORY = 'category',
  LIKES = 'likes',
  OP_LIKES = 'op_likes',
  POSTERS = 'posters'
}
