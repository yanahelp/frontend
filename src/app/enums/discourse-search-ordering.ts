export enum DiscourseSearchOrdering {
    RELEVANCE = 'relevance',
    LATEST_TOPIC = 'latest_topic',
    LATEST_MESSAGE = 'latest',
    VIEWS = 'views',
    LIKES = 'likes'
}
  