export enum DiscourseCategory {
    NO_CATEGORY = 1,
    RETOURS_SITE = 2,
    RESPONSABLES = 3, // admin
    SALON = 4 // membres avec confiance > 3
}
  