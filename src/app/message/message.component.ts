import { Component, OnInit } from '@angular/core';
import { DiscourseContentService } from '../services/discourse-content.service';
import { DiscourseAuthService } from '../services/discourse-auth.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { MessageStateService } from './message-state.service';
import { DiscourseTopic } from '../interfaces/discourse-topic';
import { DiscoursePost } from '../interfaces/discourse-post';
import { DiscourseUser } from '../interfaces/discourse-user';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
/**
 * The main component of the message module which contains the conversation list and a selected conversation
 * 
 * ![mobile view](/documentation_assets\mobile_messaging_home.png)
 */
export class MessageComponent implements OnInit {
  topics: DiscourseTopic[] = [];
  messagingUsers: DiscourseUser[] = [];
  selected: DiscourseTopic;
  session$;
  showDesktop: boolean;

  /**
   * The message component constructor
   * @param discourse the discourse content service used to retrieve the conversations
   * @param auth the discourse authentication service used to check if a user is authenticated to know what to show 
   * TODO: Implement anonymous usage and perhaps demo / test user
   */
  constructor(private discourse: DiscourseContentService, private auth: DiscourseAuthService, public breakpointOberser: BreakpointObserver, private messageState: MessageStateService) { }

  ngOnInit(): void {
    /**
     * The user session observable
     */
    this.session$ = this.auth.session;
    // TODO: Create interface for currentSession
    // TODO: Convert everything to observables promises feel inefficient to work with (Also reimplimenting the response object returned by http get method is inefficient)
    this.auth.session.subscribe(session => {
      console.log("The session has been updated with value", session);
      this.discourse.getPrivateMessages(session.user.username).then(resp => {
        if (resp.succeeded) {
          console.log(resp.response);

          this.messagingUsers = resp.response.topic_list.topics.map(topic => {
            // TODO: Handle private group conversations
            // TODO: Handle polling for new conversations
            const filtered = resp.response.users.filter(user => user.id === topic.participants[0].user_id)
            console.log(filtered);
            return filtered[0];
          })
          this.topics = resp.response.topic_list.topics;
          console.log(this.topics);
        }
      });
    });

    // Check the viewport to know which layout to display
    this.breakpointOberser
      .observe(['(min-width: 400px'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showDesktop = true;
        } else {
          this.showDesktop = false;
        }
      });

  }

  /**
   * This method is called from the {@link ContactListComponent} component embedded in the message component.
   * It recieves an event the index of the selected conversation in our topics list.
   * ![A selectable topic](/documentation_assets\mobile_messaging_select_topic.png)
   * @param index the index of the topic in the list of topics to be selcted.
   */
  selectTopic(index: number) {
    console.log('received from event', index);
    // TODO: List conversations on the right in stead of users !!!
    this.selected = this.topics[index];
    this.messageState.selectTopic(this.selected);
  }
}
