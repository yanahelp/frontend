import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageRoutingModule } from './message-routing.module';
import { MessageComponent } from './message.component';
import { ConversationComponent } from './conversation/conversation.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { InputComponent } from './input/input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListeDiscussionMobileComponent } from './liste-discussion-mobile/liste-discussion-mobile.component';
import { DiscussionItemComponent } from './discussion-item/discussion-item.component';

@NgModule({
  declarations: [MessageComponent, ConversationComponent, ContactListComponent, InputComponent, ListeDiscussionMobileComponent, DiscussionItemComponent],
  imports: [
    CommonModule,
    MessageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MessageModule { }
