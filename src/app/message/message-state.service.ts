import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DiscoursePost } from '../interfaces/discourse-post';
import { DiscourseTopic } from '../interfaces/discourse-topic';

/**
 * The service providing the selected topic value accross components.
 * TODO: Add a routing setter
 */
@Injectable({
  providedIn: 'root'
})
export class MessageStateService {
  /**
   * The currently selected converstation.
   */
  topic: BehaviorSubject<DiscourseTopic> = new BehaviorSubject(null);

  /**
   * The Message State service construtor.
   */
  constructor() { }

  /**
   * The method called from components to specify which topic was selected.
   * @param topic The selcted topid. {@link /interfaces/DiscourseTopic}
   */
  selectTopic(topic: DiscourseTopic) {
    this.topic.next(topic);
  }
}
