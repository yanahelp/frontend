import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
/**
 * A text input for sending messages 
 * TODO: reflect upon whether it makes sense to use this component in other parts of the site
 */
export class InputComponent implements OnInit {
  /**
   * The form to be sent contains only message variable which is required
   */
  sendMessageForm: FormGroup;
  @Input()
  topicId: number;

  /**
   * The Input constructor which injects the desired elements into the component
   * @param formBuilder An angular formdbuilder in order to create the sendMessge FormGroup
   * @param discourse the discourse content service for retreiveing and sending messages
   */
  constructor(private formBuilder: FormBuilder, private discourse: DiscourseContentService) {
    this.sendMessageForm = this.formBuilder.group({
      message: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  /**
   * The method that is executed upon sending of the message.
   * Retrieves the message field from the form value(s) and posts them to desired endpoint
   * using method from discourse content service TODO: Implement
   * @param formData the value(s) of the submitted form a JSON object containing only the message field
   */
  onSubmit(formData) {
    this.discourse.createPost(this.topicId, formData.message);
    this.sendMessageForm.reset();
  }

}
