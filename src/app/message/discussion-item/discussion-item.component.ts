import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DiscoursePost } from 'src/app/interfaces/discourse-post';
import { DiscourseTopic } from 'src/app/interfaces/discourse-topic';
import { DiscourseUser } from 'src/app/interfaces/discourse-user';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
import { MessageStateService } from '../message-state.service';

@Component({
  selector: 'app-discussion-item',
  templateUrl: './discussion-item.component.html',
  styleUrls: ['./discussion-item.component.css']
})
export class DiscussionItemComponent implements OnInit {

  @Input() topic: DiscourseTopic;

  @Input() user: DiscourseUser;

  messages$: Observable<DiscoursePost[]>;

  lastMessage$: Observable<string>;

  constructor(private discourse: DiscourseContentService, private messageState: MessageStateService) { }

  ngOnInit(): void {
    this.messages$ = this.discourse.pollTopic(this.topic.id).pipe(
      // tap(topic => console.log(topic)),
      map(topic => topic.post_stream.posts),
    );

    this.lastMessage$ = this.messages$.pipe(
      map(messages => messages[messages.length - 1].cooked),
    );
  }

}
