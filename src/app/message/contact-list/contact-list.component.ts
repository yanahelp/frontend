import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
import { DiscourseUser } from 'src/app/interfaces/discourse-user';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
/**
 * A list of conversations for now only private one-to-one or private group converations.
 */
export class ContactListComponent implements OnInit {
  /**
   * The list of conversations input and updated in the parent component.
   */
  @Input()
  listUser: DiscourseUser[] = [];

  /**
   * The selected conversation
   */
  @Output()
  selectedUser = new EventEmitter<number>();

  /**
   * The id of the selected conversation for local usage
   */
  selectedId: number;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Emits the id of the selected conversation and updates the local value.
   */
  updateSelected(userId: number) {
    console.log('Selected', userId);
    this.selectedUser.emit(userId);
    this.selectedId = userId;
  }
}
