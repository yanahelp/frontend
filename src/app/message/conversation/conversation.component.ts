import { Component, OnInit, Input, ViewChildren, ElementRef, QueryList, AfterViewInit } from '@angular/core';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
import { DiscourseTopic } from 'src/app/interfaces/discourse-topic';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { DiscoursePost } from 'src/app/interfaces/discourse-post';
import { MessageStateService } from '../message-state.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit, AfterViewInit {

  @ViewChildren('bubble') messageBubbles!: QueryList<ElementRef>;

  /**
   * Whether the discourse service is reachable or not. Used to display an error message.
   */
  success = true;
  /**
   * The error message received from the server
   */
  errorResponse = '';

  /**
   * An observable to contain the list of messages for the selected view.
   * An observable was chosen to ease updating the value as is rxjs/angular convention.
   *
   * The observable was removed for visual testing with logged in user
   */
  messages$: Observable<DiscoursePost[]>;
  /**
   * The actual object where the selection is stored locally.
   */
  _topic: DiscourseTopic;
  showInput: boolean;

  constructor(
    private discourse: DiscourseContentService,
    private messageState: MessageStateService,
    private breakpointOberser: BreakpointObserver
  ) { }

  ngOnInit(): void {
    this.messageState.topic.subscribe(topic => {
      this._topic = topic;
      this.messages$ = this.discourse.pollTopic(this._topic.id).pipe(
        tap(topic => console.log(topic)),
        map(topic => topic.post_stream.posts),
      );
    });

    this.breakpointOberser
      .observe(['(max-width: 400px'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showInput = true;
        } else {
          this.showInput = false;
        }
      })
  }

  ngAfterViewInit() {
    this.messageBubbles.changes.subscribe(_ => {
      if (this.messageBubbles) {
        console.log('New message scrolling into view', this.messageBubbles.last);
        this.messageBubbles.last.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
      } else {
        console.log('Messages aren\'t available yet');
      }
    });
  }

}
