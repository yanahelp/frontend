import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessageComponent } from './message.component';
import { ListeDiscussionMobileComponent } from './liste-discussion-mobile/liste-discussion-mobile.component';
import { ConversationComponent } from './conversation/conversation.component';

const routes: Routes = [{ path: '', component: MessageComponent },
{
  path: 'mobile',
  component: ListeDiscussionMobileComponent
},
{
  path: 'conversation',
  component: ConversationComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRoutingModule { }
