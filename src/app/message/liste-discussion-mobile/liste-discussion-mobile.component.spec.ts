import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDiscussionMobileComponent } from './liste-discussion-mobile.component';

describe('ListeDiscussionMobileComponent', () => {
  let component: ListeDiscussionMobileComponent;
  let fixture: ComponentFixture<ListeDiscussionMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDiscussionMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDiscussionMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
