import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DiscourseOrdering } from 'src/app/enums/discourse-ordering.enum';
import { DiscourseTopic } from 'src/app/interfaces/discourse-topic';
import { DiscourseUser } from 'src/app/interfaces/discourse-user';
import { DiscourseAuthService } from 'src/app/services/discourse-auth.service';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
import { MessageStateService } from '../message-state.service';

import {
  trigger,
  style,
  animate,
  transition,
  animateChild,
  query,
  stagger
} from '@angular/animations';

/**
 * The list of discussions for view on mobile.
 *
 * |Design initial|Implémentation|
 * ---------------|--------------------|
 * |![discussion design](/documentation_assets/mobile_messaging_home.png)|![discussion implementation](/documentation_assets/impl_mobile_messaging_home.png)|
 */
@Component({
  selector: 'app-liste-discussion-mobile',
  templateUrl: './liste-discussion-mobile.component.html',
  styleUrls: ['./liste-discussion-mobile.component.css'],
  animations: [
    // animation triggers
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }), // initial state
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)'), // timing
        style( { transform: 'scale(1)', opacity: 1}) // final state
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: 1, height: '*'}),
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.5)', opacity: 0,
            height: '0px', margin: '0px'
          }))
      ])
    ]),
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(300, animateChild()))
      ]),
      transition(':leave', [
        query('@items', stagger(300, animateChild()))
      ]),
    ])
  ]
})
export class ListeDiscussionMobileComponent implements OnInit {
  /**
   * The list of private discussions for the currently logged in user. Or publicly accessible topics
   */
  @Input()
  topics: DiscourseTopic[];

  /**
   * The list of uysers involved in discussions used to display the profile picture of the last poster.
   */
  @Input()
  messagingUsers: DiscourseUser[];

  /**
   * A user set in order to get unique users from all those involved in converasations.
   */
  userList: Set<DiscourseUser> = new Set();

  /**
   * The list of publicly accessible topics or group conversations.
   */
  publicTopics: Subject<DiscourseTopic[]>;
  // TODO: Implement merging method for public and private "messages" to be displayed in chronological order.
  // TODO: Does it make more sens to simple check whether a conversation is a group conversation or a private conversation rather than hack with forum messages?

  /**
   * A boolean flag that defines whether groups are visible or not.
   * 
   * |Design initial|Implémentation|
   * ---------------|--------------------|
   * |![discussion design](/documentation_assets/mobile_messaging_home.png)|![discussion implementation](/documentation_assets/impl_mobile_messaging_home.png)|
   * As we can see in the design all discussions are in chronological order. Whereas in the implementation individual messages come before
   */
  groupSelected = false;

  /**
   * A boolean flag that defines whether groups are visible or not.
   * 
   * |Design initial|Implémentation|
   * ---------------|--------------------|
   * |![experts design](/documentation_assets/mobile_expert_filter.png)|![experts implementation](/documentation_assets/impl_mobile_expert_filter.png)|
   */
  expertSelected = true;

  /**
   * The message list constructor.
   * @param messageState The service that will store the selected topic accross this app. {@link MessageStateService}
   * @param auth The authentication service which gives access to the authenticated user's session.
   * @param discourse The discourse service used to query conversations.
   */
  constructor(public messageState: MessageStateService, public auth: DiscourseAuthService, public discourse: DiscourseContentService) { }

  /**
   * The Life state management method used to initialize the list of conversations.
   */
  ngOnInit(): void {
    this.discourse.getLatestTopicsByOrder(DiscourseOrdering.DEFAULT).then( res => {
      this.publicTopics = res.response.topic_list.topics;
      res.response.users.forEach(u => this.userList.add(u));
    });
  }

  /**
   * The method used to get all of a users' information.
   * @param username The user for which we want more information.
   */
  getUser(username: string): DiscourseUser {
    this.messagingUsers.forEach(u => this.userList.add(u));
    if ( username !== this.auth.currentUser.username) {
      const res = Array.from(this.userList).find(user => user.username === username);
      return res;
    } else {
      return this.auth.currentUser;
    }
  }
}
