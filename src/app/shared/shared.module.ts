import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmergencyBoxComponent } from '../sub-components/emergency-box/emergency-box.component';
import { RouterModule } from '@angular/router';
import { ClickOutsideDirective } from '../directives/click-outside.directive';
import { CantonboxComponent } from '../sub-components/cantonbox/cantonbox.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/**
 * Module listing components to be shared between multiple modules
 * (as a component can only be declared once)
 */
@NgModule({
  //TODO create multiple sharing modules for different uses
  declarations: [
    EmergencyBoxComponent,
    ClickOutsideDirective,
    CantonboxComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    EmergencyBoxComponent,
    ClickOutsideDirective,
    CantonboxComponent
  ]
})
export class SharedModule { }
