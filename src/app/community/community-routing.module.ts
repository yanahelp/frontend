import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopicViewComponent } from './topic-view/topic-view.component';
import { SearchViewComponent } from './search-view/search-view.component';
import { CommunityComponent } from './community.component';
import { CreateViewComponent } from './create-view/create-view.component';

const routes: Routes = [
        {
            path: '',
            component: CommunityComponent,
            children: [
                {
                    path: '',
                    component: SearchViewComponent,
                    /**
                     * Identification for the animations
                     */
                    data: {animation: 'SearchPage'} 
                },
                {
                    path: 'topic-view/:id',
                    component: TopicViewComponent,
                    data: {animation: 'TopicPage'}
                },
                {
                    path: 'create-view',
                    component: CreateViewComponent,
                    data: {animation: 'CreateTopicPage'}
                }
            ]
        }
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule { }
