import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animations';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css'],
  animations: [
    slideInAnimation
  ]
})
export class CommunityComponent implements OnInit {

  constructor() {}

  /**
   * Method preparing the animations for the navigation transition (taken from the Angular documentation)
   * @param outlet the target router outlet to animate
   */
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  ngOnInit(): void {
    
  }

}
