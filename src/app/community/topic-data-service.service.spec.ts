import { TestBed } from '@angular/core/testing';

import { TopicDataServiceService } from './topic-data-service.service';

describe('TopicDataServiceService', () => {
  let service: TopicDataServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopicDataServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
