import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunityComponent } from './community.component';
import { SearchViewComponent } from './search-view/search-view.component';
import { TopicViewComponent } from './topic-view/topic-view.component';
import { TopicBubbleComponent } from '../sub-components/topic-bubble/topic-bubble.component';
import { PostBubbleComponent } from '../sub-components/post-bubble/post-bubble.component';
import { TopicMainBubbleComponent } from '../sub-components/topic-main-bubble/topic-main-bubble.component';
import { CommunityRoutingModule } from './community-routing.module';
import { SearchBarComponent } from '../sub-components/search-bar/search-bar.component';
import { DropdownComponent } from '../sub-components/dropdown/dropdown.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubmitBoxComponent } from '../sub-components/submit-box/submit-box.component';
import { CreateViewComponent } from './create-view/create-view.component';
import { ArrowComponent } from '../sub-components/arrow/arrow.component';
import { PaginationComponent } from '../sub-components/pagination/pagination.component';
import { TextFormattingService } from './text-formatting.service';
import { TopicDataService } from './topic-data-service.service';
import { LoginAlertComponent } from '../sub-components/login-alert/login-alert.component';
import { ImageButtonComponent } from '../sub-components/image-button/image-button.component';

@NgModule({
  declarations: [
    CommunityComponent,
    SearchViewComponent,
    TopicViewComponent,
    TopicBubbleComponent,
    PostBubbleComponent,
    TopicMainBubbleComponent,
    SearchBarComponent,
    DropdownComponent,
    SubmitBoxComponent,
    CreateViewComponent,
    ArrowComponent,
    PaginationComponent,
    LoginAlertComponent,
    ImageButtonComponent
  ],
  imports: [
    CommonModule,
    CommunityRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    TextFormattingService,
    TopicDataService
  ]
})
export class CommunityModule {
  
}
