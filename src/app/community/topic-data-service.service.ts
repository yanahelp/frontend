import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DiscourseSearchOrdering } from '../enums/discourse-search-ordering';

@Injectable()
/**
 * Service keeping in memory the state of the last search, so that the user can continue browsing 
 * where he left off after having clicked on a topic or left to another tab
 */
export class TopicDataService {

  /**
   * an object consisting of the last search term (null if no search), the order and the page
   * by default the order is by relevance
   */
  private searchViewState = new BehaviorSubject<[string, DiscourseSearchOrdering, number]>([null, DiscourseSearchOrdering.RELEVANCE, 0]);
  /**
   * creating an observable to get the values
   */
  currentSearchViewState = this.searchViewState.asObservable();

  /**
   * TopicData service constructor
   */
  constructor() { }

  /**
   * Method to update the values of the state of search
   * @param state the new state
   */
  changeState(state: [string, DiscourseSearchOrdering, number]): void {
    this.searchViewState.next(state);
  }

}