import { Injectable } from '@angular/core';
import { TimeConversionService } from "../services/time-conversion.service";

@Injectable()
/**
 * Service used to format the date for display and adjusting the link for the avatar image
 */
export class TextFormattingService {

  /**
   * Textformatting service constructor
   * @param timeConversion service to convert the time in a more readable text
   */
  constructor(private timeConversion: TimeConversionService) { }

  /**
   * Method formatting a date description (used for posts and topics for example)
   * @param creationDate date to display
   * @param author author to display next to the date
   * @returns a ready-made description with a date and an author
   */
  descriptionFormatting(creationDate: string, author: string): string {
    return 'posted ' + this.timeConversion.calculateDiff(creationDate) + ' ago by ' + author;
  }

  /**
   * Method completing the avatar link received by the discord API
   * @param link the original link
   * @returns a correct and usable link to the image
   */
  avatarLinkFormatting(link: string): string {
    /**
     * the size defines the size of the image, can be altered if needed
     */
    return 'https://discourse.yana.help/' + link.replace('{size}', '100');
  }

}
