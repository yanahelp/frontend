import { trigger, transition, style, query, animateChild, animate, group } from '@angular/animations';
/**
 * Animations used in the community tab (forum)
 */
export const slideInAnimation =
  trigger('routeAnimations', [
    /**
     * Left to right transition
     */
    transition('TopicPage => SearchPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [ // Style common for both pages at the start // Applique les styles aux pages : enter la page qui vient :leave celle qui part
        style({ // centre
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '-100%' }) // placing the new page on the left outside of view //slide à gauche
      ]),
      query(':leave', animateChild()),
      group([ // grouping simultaneous animations
        query(':leave', [
          animate('300ms ease-out', style({ left: '100%' })) // removing old page
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' })) // placing new page
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    /**
     * Transition from left to right
     */
    transition('SearchPage => TopicPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ left: '-100%' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ]),

    // dupplication à voir si par la suite on peut faire une méthode réutilisable pour les transitions
    /**
     * Transition from right to left
     */
    transition('CreateTopicPage => SearchPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '-100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ left: '100%' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    /**
     * Transition from left to right
     */
    transition('SearchPage => CreateTopicPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('300ms ease-out', style({ left: '-100%' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    /**
     * Transition from top to bottom
     */
    //TODO fix weird stuttering when no scrollTop=0
    transition('CreateTopicPage => TopicPage', [ // amene en bas de la page directement lorsqu on créer un nouveau poste
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':leave', [
        style({ height: '150vh'})
      ]),
      query(':enter', [
        style({ top: '150vh' }) //marche pas en 100% et 150 comme la page est plus grande
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('400ms ease-out', style({ top: '-150vh' }))
        ]),
        query(':enter', [
          animate('400ms ease-out', style({ top: '0vh' }))
        ])
      ]),
      query(':enter', animateChild()),
    ])
  ]);