import { Component, OnInit } from '@angular/core';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
import { DiscourseTopic } from 'src/app/interfaces/discourse-topic';
import { TopicDataService } from '../topic-data-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DiscoursePost } from 'src/app/interfaces/discourse-post';
import { DiscourseAuthService } from 'src/app/services/discourse-auth.service';
/**
 * Component that allows a user to create a new topic in the forum
 */
@Component({
  selector: 'app-create-view',
  templateUrl: './create-view.component.html',
  styleUrls: ['./create-view.component.css']
})

export class CreateViewComponent implements OnInit {

  session$;

  /**
   * Create-view component constructor
   * @param discourseContent the discourse content service used here to create a new topic
   * @param router the router used to navigate between pages
   * @param route the object containing information about the current route
   * @param auth the discourse authentication service used to check if a user is authenticated to know what to show
   * TODO: Implement anonymous usage and perhaps demo / test user
   */
  constructor(
    private discourseContent: DiscourseContentService,
    private router: Router,
    private route: ActivatedRoute,
    private auth: DiscourseAuthService) { }

  ngOnInit(): void {
    /**
     * The user session observable
     */
    this.session$ = this.auth.session;
  }

  /**
   * Method to create a new topic
   * @param data 
   */
  onSubmit(data: any){
    this.discourseContent.createTopic(data.title, data.body, 1).then(
      res => {
        console.log(res);
        if(res.succeeded) {
          /**
           * navigating to the topic view of the newly created topic
           */
          this.router.navigate(['topic-view', (res.response as DiscoursePost).topic_id], {relativeTo: this.route.parent, skipLocationChange: true});
        }
      }
    )
  }

}
