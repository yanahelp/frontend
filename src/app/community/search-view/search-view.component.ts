import { Component, OnInit, OnDestroy } from '@angular/core';
import { DiscourseContentService } from "../../services/discourse-content.service";
import { DiscourseOrdering } from "../../enums/discourse-ordering.enum";
import { DiscourseResponse } from "../../interfaces/discourse-response";
import { DiscourseTopic } from "../../interfaces/discourse-topic";
import { TextFormattingService } from "../text-formatting.service";
import { DiscourseTopicShort } from 'src/app/interfaces/discourse-topic-short';
import { TopicDataService } from '../topic-data-service.service';
import { take } from 'rxjs/operators';
import { DiscourseSearchOrdering } from 'src/app/enums/discourse-search-ordering';
/**
 * Component displaying a page used to browse through the different topics that have been created
 */
@Component({
  selector: 'app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.css']
})

export class SearchViewComponent implements OnInit, OnDestroy {

  //TODO: cleanup and reorganize code, some methods should have parameters and be distinct

  /**
   * Styles to pass as input to the suggestion buttons
   */
  createStyle: any = {
    'color': 'white',
    'font-size': 'larger'
  }
  searchSuggestionStyle: any = {
    box: {
      'min-height': '10vw'
    },
    text: {
      'font-size': 'larger'
    }
  }

  displayedTopics: DiscourseTopic[]; // affiche les sujet de la page courrante
  currentTopics: DiscourseTopicShort[][]; // affiche la liste de sujet (tout les topic recherché meme ceux sur les autres pages)
  // serverResponse: DiscourseTopicCollection; 
  searchedTopics: DiscourseTopic[]; //TODO: implement request for trending searches (le but afficher les recherches les plus récentes des utilisateurs)

  /**
   * Different options deciding to show or not to show parts of the html
   */
  show: boolean = false; // show or not pagination
  noResults: boolean = false; // display no results found

  /**
   * Current search parameters kept in memory
   */
  currentOrder: DiscourseSearchOrdering = DiscourseSearchOrdering.RELEVANCE;
  currentSearch: string = null;
  currentPage: number = 0;

  /**
   * String describing which topics are currently displayed (result of a search or latest topics)
   */
  displayInfo: string = null;

  /**
   * Search-view component constructor
   * @param discourseContent the discourse content service used here to create a new topic
   * @param textFormatting service to format the date of a post
   * @param topicDataService service to save and keep in memory the search parameters
   */
  constructor(
    private discourseContent: DiscourseContentService,
    public textFormatting: TextFormattingService,
    private topicDataService: TopicDataService) {
  }

  /**
   * Method to save the searched value and trigger search request
   * @param value the term searched
   */
  onSearch(value: string): void{
    this.currentSearch = value;
    this.search();
  }

  /**
   * Method to save the order value of the search and trigger search request
   * @param value order chosen for the search
   */
  onFilter(value: DiscourseSearchOrdering): void {
    if(this.currentSearch==null){
      this.currentSearch="";
    }
    this.currentOrder = value;
    this.search();
  }

  /**
   * Method launching a request for a search
   */
  async search(): Promise<void> {
    /**
     * putting up loading message and removing topics displayed
     */
    this.show = false; // affiche le loading
    this.noResults = false; // si true message d'erreur
    this.displayedTopics = []; // vide la recherche précédente

    /**
     * Met à jour la description de la recherche
     */
    this.displayInfo = "Search results for :  \"" + this.currentSearch + "\"";

    /**
     * Lance la recherche grâce au service
     */
    this.discourseContent.search(this.currentSearch, this.currentOrder).then(
      (data: DiscourseResponse) => {
        if(data.succeeded && data.response.topics != undefined) {
          this.updateDisplayedTopics(data.response.topics).then(
            () => this.show = true
          );
        }else {
          this.noResults = true;
        }
      }
    );
  }

  /**
   * Method used to divide a list of topics into pages
   * @param topicsList list of topics to reorganize
   * @returns a two dimensional array of topics (book of topics)
   */
  private dividePages(topicsList: DiscourseTopicShort[]): DiscourseTopicShort[][] {
    let res: DiscourseTopicShort[][] = [];
    /**
     * chunk is the number of topics to display per page
     */
    let chunk: number = 3;
    for (let i = 0; i < topicsList.length; i+=chunk) {
      res.push(topicsList.slice(i,i+chunk));
    }
    return res
  }

  /**
   * Method to display topics on another page
   * @param page the page to display
   */
  onPageChange(page: number): void{
    /**
     * display loading
     */
    this.show = false;
    this.displayedTopics = [];

    this.currentPage = page;
    this.getTopicsInformation(this.currentTopics[page]).then(
      topics => {
        this.displayedTopics = topics;
        this.show = true;
      }
    );
  }

  /**
   * Main method updating the general display of topics from a simple list of topics
   * @param topics list of topics with shortened information
   */
  private async updateDisplayedTopics(topics: DiscourseTopicShort[]): Promise<void> {
    /**
     * dividing the content into multiple pages and loading content for one only to lessen and limit
     *  the load of data to retrieve (less time)
     */
    this.currentTopics = this.dividePages(topics);
    await this.getTopicsInformation(this.currentTopics[this.currentPage]).then(
      topics => {
        this.displayedTopics = topics;
      }
    );
  }

  /**
   * Method to get the complete information of each topic in a list (DiscourseTopicShort --> DiscourseTopic)
   * @param topics the list of topics with shortened information
   * @returns a list of the same topics with complete information
   */
  private async getTopicsInformation(topics: DiscourseTopicShort[]): Promise<DiscourseTopic[]>{
    let displayedTopics: DiscourseTopic[] = [];
    for(const topic of topics){
      await this.discourseContent.getTopic(topic.id).then(
        data => {
          if(data.succeeded) displayedTopics.push(data.response);
        }
      )
    }
    return displayedTopics
  }

  /**
   * Method to save the state of the search
   * @param topicType the search parameter
   * @param order the order of search
   * @param page the page to save
   */
  transferData(topicType: string, order: DiscourseSearchOrdering, page: number): void {
    this.topicDataService.changeState([topicType, order, page]);
  }

  /**
   * Method to get all the latest topics
   */
  async getAllTopics(){
    this.displayInfo = "Latest topics";
    /**
     * display loading
     */
    this.currentSearch = null;
    this.show = false;
    this.noResults = false;
    this.displayedTopics = [];
    //TODO implement a way to order without searching, i.e. most popular post of all time
    await this.discourseContent.getLatestTopicsByOrder(DiscourseOrdering.CREATED).then(
      (data: any) => {
        if(data.succeeded) {
          // this.serverResponse = data.response;
          this.updateDisplayedTopics(data.response.topic_list.topics).then(
            () => this.show = true
          )
        }
      }
    );
  }

  ngOnInit(): void {
    /**
     * get the saved state of the search
     */
    this.topicDataService.currentSearchViewState.pipe(take(1)).subscribe(
        state => {
          this.currentOrder = state[1];
          this.currentPage = state[2];
          /**
           * if no search, display latest topics
           * else display previous search
           */
          if(state[0] == null) this.getAllTopics();
          else this.onSearch(state[0]);
        }
    );
  }

  ngOnDestroy(): void{
    /**
     * on navigation to other route, save state of display
     */
    this.transferData(this.currentSearch, this.currentOrder, this.currentPage);
  }

}
