import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiscourseContentService } from '../../services/discourse-content.service'
import { DiscourseTopic } from "../../interfaces/discourse-topic";
import { DiscourseResponse } from "../../interfaces/discourse-response";
import { TextFormattingService } from "../text-formatting.service";
import { DiscoursePost } from 'src/app/interfaces/discourse-post';
import { DiscourseAuthService } from 'src/app/services/discourse-auth.service';
/**
 * Component displaying the posts of a certain topic
 */
@Component({
  selector: 'app-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.css']
})

export class TopicViewComponent implements OnInit {

  session$;

  /**
   * id of the topic to display
   */
  private id: number;

  /**
   * display loading
   */
  show: boolean = false;

  /**
   * all the information needed to display
   */
  topic: DiscourseTopic;
  mainPost: DiscoursePost;
  posts: DiscoursePost[];

  /**
   * Topic-view component constructor
   * @param route current route information
   * @param discourseContent discourse service to retrieve the topic and publish a post
   * @param textFormatting service to format the date of a post and the avatar image link
   * @param auth the discourse authentication service used to check if a user is authenticated to know what to show
   */
  constructor(private route: ActivatedRoute,
    private discourseContent: DiscourseContentService,
    public textFormatting: TextFormattingService,
    private auth: DiscourseAuthService) { }

  /**
   * Method to retrieve information on a topic and reorganize it
   * @param id id of the topic
   */
  requestTopic(id: number): void {
    this.discourseContent.getTopic(id).then(
      (data: DiscourseResponse) => {
        if(data.succeeded) {
          this.topic = data.response;
          this.posts = this.topic.post_stream.posts;
          /**
          * isolating the first post in order to use a different display for it in the html
          */
          this.mainPost = this.posts.shift();
          this.show = true;
        }
        else console.log(data.response); //TODO: handle error message
      }
    );
  }

  /**
   * Method creating a new post
   * @param data body of the post
   */
  onSubmit(data: string): void {
    console.log(data);
    this.discourseContent.createPost(this.topic.id, data).then(
      res => {
        /**
         * pushing the new post in the list of posts to display
         */
        if(res.succeeded) this.posts.push(res.response);
        else console.log(res.response); //TODO: handle error message
      }
    );
  }

  /**
   * Method removing empty posts (which are actions)
   * @returns filtered list of posts
   */
  filterPosts(): DiscoursePost[]{
    //TODO: implement actions visually ? (pinned, ...)
    return this.posts.filter(x => x.cooked.length > 0);
  }

  ngOnInit(): void {
    /**
     * The user session observable
     */
    this.session$ = this.auth.session;
    /**
     * getting the id passed in the url
     */
    this.route.params.subscribe(params => {
      this.id = +params.id; // (+) converts string 'id' to a number
      this.requestTopic(this.id);
    });
  }
  
}
