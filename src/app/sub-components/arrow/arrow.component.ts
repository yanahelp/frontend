import { Component, OnInit, Input } from '@angular/core';
/**
 * Component displaying an arrow that moves on hover
 */
@Component({
  selector: 'app-arrow',
  templateUrl: './arrow.component.html',
  styleUrls: ['./arrow.component.css']
})
export class ArrowComponent implements OnInit {

  /**
   * route to navigate to on click
   */
  @Input() route: string; //TODO: remove the navigating option, so that the parent can set that option

  /**
   * Arrow component constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
