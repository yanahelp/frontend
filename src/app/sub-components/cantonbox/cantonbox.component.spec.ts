import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CantonboxComponent } from './cantonbox.component';

describe('CantonboxComponent', () => {
  let component: CantonboxComponent;
  let fixture: ComponentFixture<CantonboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CantonboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CantonboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
