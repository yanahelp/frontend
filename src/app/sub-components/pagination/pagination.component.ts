import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
/**
 * Pagination system to navigate between pages
 */
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})

export class PaginationComponent implements OnInit {

  /**
   * to prevent to navigate to non existent pages
   */
  @Input() maxPages: number;
  /**
   * to initialize the pagination at a certain page
   */
  @Input() currentPage: number;

  /**
   * Output to send the page to the parent
   */
  @Output() pageChange = new EventEmitter<number>();

  /**
   * Pagination compnent constructor
   */
  constructor() { }

  /**
   * Method to update the current page counter and send it to the parent
   * @param value +1 or -1 to go to the next or previous page respectively
   */
  changePage(value: number): void {
    /**
     * Condition to not go below 0 or over the max page limit
     */
    if((value + this.currentPage >= 0) && (value + this.currentPage < this.maxPages)){
      this.currentPage += value;
      this.pageChange.emit(this.currentPage);
    }
  }

  ngOnInit(): void {
  }

}
