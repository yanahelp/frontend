import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
/**
 * Component displaying an input searchbar
 */
@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent implements OnInit {
 //TODO add a button to validate the search ? (@Sara je suis pour qu on laisse juste avec le enter)
  /**
   * communicate with parent
   */
  @Output() messageEvent = new EventEmitter<string>();
  /**
   * predefined input if needed 
   * valeur par defaut de depart
   */
  @Input() automaticFill: string;

  /**
   * SearchBar component constructor
   */
  constructor() { }

  /**
   * send the current value of the searchbar to the parent
   * @param event the input in the searchbar
   */
  sendToParent(event: any): void {
    this.messageEvent.emit(event.target.value);
  }

  ngOnInit(): void {
  }

}
