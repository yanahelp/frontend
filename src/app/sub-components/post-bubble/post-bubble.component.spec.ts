import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostBubbleComponent } from './post-bubble.component';

describe('PostBubbleComponent', () => {
  let component: PostBubbleComponent;
  let fixture: ComponentFixture<PostBubbleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostBubbleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
