import { Component, OnInit, Input } from '@angular/core';
/**
 * Template for a post bubble
 */
@Component({
  selector: 'app-post-bubble',
  templateUrl: './post-bubble.component.html',
  styleUrls: ['./post-bubble.component.css']
})

export class PostBubbleComponent implements OnInit {

  /**
   * avatar image link
   */
  @Input() image: string;
  /**
   * description on top in gray
   */
  @Input() description: string;
  /**
   * main text inside the bubble
   */
  @Input() body: string;

  /**
   * PostBubble component constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
