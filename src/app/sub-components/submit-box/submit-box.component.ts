import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { DiscourseContentService } from 'src/app/services/discourse-content.service';
/**
 * A submission box in 2 styles with: 
 * 0. title, body fields and a submit button in seperate containers
 * 1. body field and submit button inside the same container
 */
@Component({
  selector: 'app-submit-box',
  templateUrl: './submit-box.component.html',
  styleUrls: ['./submit-box.component.css']
})

export class SubmitBoxComponent implements OnInit {
//TODO: maybe separate into 2 components ?
  /**
   * Access to the html elements to read their content,
   * chose not to use Input or Textarea fields as they don't grow with its content
   */
  @ViewChild('title', {read: ElementRef}) title: ElementRef;
  @ViewChild('body', {read: ElementRef}) body: ElementRef;

  /**
   * the style you choose 0 or 1
   */
  @Input() type: number;
  /**
   * height of the body field container
   */
  @Input() height: string;

  /**
   * communication with parent
   */
  @Output() messageEvent = new EventEmitter<any>();

  /**
   * SubmitBox component constructor
   */
  constructor() {}

  /**
   * Method to send the content of the fields to the parent
   */
  onSubmit(): void {
    /**
     * for type 0 we need to send the title content too
     */
    if(!this.type) this.messageEvent.emit({title: this.title.nativeElement.innerText, body: this.body.nativeElement.innerText});
    else this.messageEvent.emit(this.body.nativeElement.innerText);
  }

  ngOnInit(): void {
  }

}
