import { Component, OnInit, Input } from '@angular/core';
/**
 * Variation of the PostBubble component with an image avatar for the first post of a topic
 */
@Component({
  selector: 'app-topic-main-bubble',
  templateUrl: './topic-main-bubble.component.html',
  styleUrls: ['./topic-main-bubble.component.css']
})
export class TopicMainBubbleComponent implements OnInit {

  @Input() topic: string;
  /**
   * link to the image
   */
  @Input() image: string;
  @Input() description: string;
  @Input() body: string;

  /**
   * TopicMainBubble component constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
