import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicMainBubbleComponent } from './topic-main-bubble.component';

describe('TopicMainBubbleComponent', () => {
  let component: TopicMainBubbleComponent;
  let fixture: ComponentFixture<TopicMainBubbleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicMainBubbleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicMainBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
