import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { DiscourseSearchOrdering } from 'src/app/enums/discourse-search-ordering';
/**
 * Component displaying a dropdown menu
 */
@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})

export class DropdownComponent implements OnInit {
  //TODO: make the dropdown options an input

  /**
   * input to display a predefined selected option
   */
  @Input() automaticFill: DiscourseSearchOrdering = undefined;
  /**
   * output to communicate with the parent component
   */
  @Output() messageEvent = new EventEmitter<DiscourseSearchOrdering>();

  /**
   * options to display as being selectable
   */
  options: string[] = ['Relevance', 'Latest Topic', 'Latest Message', 'Views', 'Likes'];
  /**
   * the real value to of the options to send back to the parent
   */
  optionsValue: DiscourseSearchOrdering[] = [
    DiscourseSearchOrdering.RELEVANCE, 
    DiscourseSearchOrdering.LATEST_TOPIC,
    DiscourseSearchOrdering.LATEST_MESSAGE,
    DiscourseSearchOrdering.VIEWS,
    DiscourseSearchOrdering.LIKES  
  ];
  /**
   * decides to show or not to show the dropdown menu
   */
  show: boolean = false;
  /**
   * value displayed as being selected
   */
  selectedOption: string = 'Order by...';

  /**
   * Dropdown component constructor
   */
  constructor() { }

  /**
   * Method changing the selected option and sending the info to the parent
   * @param i 
   */
  change(i: number): void {
    this.selectedOption = this.options[i];
    this.messageEvent.emit(this.optionsValue[i]);
  }

  ngOnInit(): void {
    /**
     * If there is an automatic fill, search for the display value
     * Depuis le parent on a seulement la vrai valeur de l'option -> tableau optenir la valeur à afficher grace à la valeur de requete
     */
    if(this.automaticFill != undefined){
      this.selectedOption = this.options[this.optionsValue.indexOf(this.automaticFill)];
    }
  }

}
