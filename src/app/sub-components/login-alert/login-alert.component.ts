import { Component, OnInit, Input } from '@angular/core';
/**
 * Component that displays an alert button that redirects to the login page
 */
@Component({
  selector: 'app-login-alert',
  templateUrl: './login-alert.component.html',
  styleUrls: ['./login-alert.component.css']
})

export class LoginAlertComponent implements OnInit {

  /**
   * text to display inside the button
   */
  @Input() body: string;

  /**
   * LoginAlert component constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
