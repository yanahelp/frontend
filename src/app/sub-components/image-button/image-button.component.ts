import { Component, Input, OnInit } from '@angular/core';
/**
 * Component that displays a button with text and an image on hover
 */
@Component({
  selector: 'app-image-button',
  templateUrl: './image-button.component.html',
  styleUrls: ['./image-button.component.css']
})

export class ImageButtonComponent implements OnInit {
//TODO: limit the number of Inputs and restrict the styling ?
  
  @Input() color: string;
  /**
   * Link to the image to display on hover
   */
  @Input() imageSrc: string;
  @Input() body: string;

  /**
   * Styling for button and text
   */
  @Input() boxStyle: any;
  @Input() textStyle: any;

  /**
   * ImageButton component constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
