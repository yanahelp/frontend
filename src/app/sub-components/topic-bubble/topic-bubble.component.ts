import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
/**
 * Template for a topic bubble
 */
@Component({
  selector: 'app-topic-bubble',
  templateUrl: './topic-bubble.component.html',
  styleUrls: ['./topic-bubble.component.css'],
  /**
   * this allows to apply style for InnerHTML elements 
   */
  encapsulation: ViewEncapsulation.None
})

export class TopicBubbleComponent implements OnInit {
//TODO fix hover when hovering on the left edge of the component
// maybe remove the hover animation from the component itself and let the parent handle it ?

  @Input() topic: string;
  @Input() description: string;
  @Input() body: string;
  @Input() replies: number;

  finalBody: string;

  /**
   * Method to remove html tags from a text and return a shortened version of the text
   * @param text text to format
   */
  shorten(text: string): string {
    //TODO make this a service ?
    var plainText = text.replace(/<[^>]*>/g, ""); // enleve les balises, reject
    let maxCharacters = 200; // limite la taille du texte à 100 caractères
    if(plainText.length < maxCharacters) return "<p>" + plainText + "</p>" 
    else return "<p>" + plainText.slice(0, maxCharacters - 1) + "..." + "</p>"
  }

  /**
   * TopicBubble component constructor
   */
  constructor() { }

  ngOnInit(): void {
    this.finalBody = this.shorten(this.body);
  }

}
