import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LangService } from '../services/lang.service';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DiscourseAuthService } from '../services/discourse-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: string;


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private lang: LangService,
    private authService: FirebaseAuthService,
    private discourseAuth: DiscourseAuthService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    // this.authService.signInUser(email, password).then(
    //   () => {
    //     this.router.navigate(['/']);
    //   },
    //   (error) => {
    //     this.errorMessage = error;
    //   }
    // );

    this.discourseAuth.login(email, password);
  }






  getUserDetails() {
    //post this details to API server return info if correct
  }

  loginUser(event) {
    event.preventDefault()
    // const target = event.target
    // const username = target.getElementById('usename')
    // const password = target.getElementById('password')
    console.log(event)
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }


}
