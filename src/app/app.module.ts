import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { MapsComponent } from './maps/maps.component';
import { ProfessionalContactComponent } from './professional-contact/professional-contact.component';
import { LoginComponent } from './login/login.component';
import { ArticleComponent } from './article/article.component';
import { PreventiveMeasuresComponent } from './preventive-measures/preventive-measures.component';
import { SignupComponent } from './signup/signup.component';

// Internationalization libraries
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LOCALE_ID } from '@angular/core';
import { NavButtonComponent } from './header/nav-button/nav-button.component';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

// CDK
import { LayoutModule } from '@angular/cdk/layout';

const firebaseConfig = {
  apiKey: 'AIzaSyAqd_QpGCW3GzW60dotvvPve5jlfjhYcQs',
  authDomain: 'yana-help.firebaseapp.com',
  databaseURL: 'https://yana-help.firebaseio.com',
  projectId: 'yana-help',
  storageBucket: 'yana-help.appspot.com',
  messagingSenderId: '678508749150',
  appId: '1:678508749150:web:750fa6f42a7eaf94ce6fcf',
  measurementId: 'G-ND6M1SKPQF'
};

export class DynamicLocaleId extends String {
  locale: string;

  toString() {
    return this.locale;
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { GeocodeService } from './services/geocode.service';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { EuropeanDatePipe } from './community/european-date.pipe';
import { HorizontalHeaderComponent } from './horizontal-header/horizontal-header.component';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GettingStartedComponent,
    MapsComponent,
    ProfessionalContactComponent,
    PreventiveMeasuresComponent,
    ArticleComponent,
    NavButtonComponent,
    LoginComponent,
    SignupComponent,
    ResetpasswordComponent,
    ThankyouComponent,
    EuropeanDatePipe,
    HorizontalHeaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD9crBwGrLUT3_iH_wi1ZqKNhGhwjzHp5I'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    SharedModule,
    LayoutModule, // layout
  ],
  providers: [
    { provide: LOCALE_ID, useClass: DynamicLocaleId },
    GoogleMapsAPIWrapper,
    GeocodeService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
